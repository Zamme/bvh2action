bl_info = {
    "name": "BVH2Action",
    "author": "Jaume Castells",
    "version": (1,0),
    "blender": (2, 79, 0),
    "category": "Object",
}


import bpy


class ArmatureObject(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Armature Object Name", default="Unknown")
    value = bpy.props.StringProperty(name="Name Value", default="Unknown")

class PoseBoneListItem(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty()

class BVH2ActionPanel(bpy.types.Panel):
    """BVH2Action Panel"""
    bl_label = "BVH2Action Panel"
    bl_idname = "SCENE_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "BVH2Action"
    #bl_context = "object"
    
    def frames_limits_update(self, context):
        if context.scene.origin_armature != "":
            print("Frames limits updating...")
            try:
                # Get origin action with orgin armature name
                origin_action = context.blend_data.actions[context.scene.origin_armature]
            except:
                print("No action in", context.scene.origin_armature)
            else:
                print("Origin action", origin_action, "found")
                context.scene.frame_start = origin_action.frame_range[0]
                context.scene.frame_end = origin_action.frame_range[1]
                # TODO: Recommend Action Keyframes?
                #action_length = context.scene.frame_end - context.scene.frame_start

    bpy.types.Scene.origin_armature = bpy.props.StringProperty(name="BVH Armature", update=frames_limits_update)
    bpy.types.Scene.target_armature = bpy.props.StringProperty(name="Target Armature")
    bpy.types.Scene.imported_action_name = bpy.props.StringProperty(name="Action Name", default="ActionName")
    bpy.types.Scene.imported_action_keyframes = bpy.props.IntProperty(name="Action Keyframes Step", default=10)
    
    def draw(self, context):
        layout = self.layout

        scene = context.scene
        
        row = layout.row()
        box0 = row.box()
        
        # Box Title
        box0.label(text="Armatures objects:")
        
        # Origin bvh armature
        box0.prop_search(scene, "origin_armature", scene, "armature_objects")
        
        # Target armature
        box0.prop_search(scene, "target_armature", scene, "armature_objects")

        # Update armatures list button
        box0.operator("scene.update_armature_objects_operator")

        row = layout.row()
        box3 = row.box()
        
        # Box Title
        box3.label(text="Bones:")
        
        # Bones List
        for t_bone_name in context.scene.target_bones:
            row = box3.row()
            row.label(text=t_bone_name.name)
            row.label(text="->")
            
        
        row = layout.row()
        box2 = row.box()
        
        # Box Title
        box2.label(text="Animation:")
        
        # Animation length
        box2.prop(scene, "frame_start")
        box2.prop(scene, "frame_end")
        
        # Imported animation keyframes
        box2.prop(scene, "imported_action_keyframes")

        # Target action name
        box2.prop(scene, "imported_action_name")
        
        row = layout.row()
        box1 = row.box()
        
        # Box Title
        box1.label(text="Operators:")
        
        # Read armatures
        box1.operator("scene.read_armatures_operator")
        
        # Add constraints
        box1.operator("scene.add_armature_constraints_operator")
        
        # Add action
        box1.operator("scene.add_action_operator")
        
        # Copy action
        box1.operator("scene.copy_action_operator")
        
        # Transfer
        row = layout.row()
        row.scale_y = 3.0
        row.operator("scene.bvh2action_transfer_operator")
    

class BVH2ActionTransferOperator(bpy.types.Operator):
    """BVH2Action Transfer Operator"""
    bl_idname = "scene.bvh2action_transfer_operator"
    bl_label = "Auto-Transfer"

    @classmethod
    def poll(cls, context):
        # NOT WORKING
        return context.scene.origin_armature != "" and context.scene.target_armature != ""
    
    def main(self, context):
        bpy.ops.scene.read_armatures_operator()
        bpy.ops.scene.add_armature_constraints_operator()
        bpy.ops.scene.add_action_operator()
        bpy.ops.scene.copy_action_operator()

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}
    
    """
    def get_datapath_translation(self, context, origin_string):
        origin_string_strings = origin_string.split('"')
        try:
            translated = self.bones_names[origin_string_strings[1]]
        except:
            print("Error translating", origin_string)
            back_string = "Error"
        else:
            parts = (origin_string_strings[0], translated, origin_string_strings[2])
            back_string = '"'.join(parts)
        return back_string
    """


class AddActionOperator(bpy.types.Operator):
    """Add Action Operator"""
    bl_idname = "scene.add_action_operator"
    bl_label = "Add Action"

    def add_action(self, context):
        it_exists = False
        for ac in context.blend_data.actions:
            if ac.name == context.scene.imported_action_name:
                it_exists = True
        if not it_exists:
            print("Adding action...")
            bdata = context.blend_data
            bdata_actions = bdata.actions
            bdata_actions.new(context.scene.imported_action_name)
            print(context.scene.imported_action_name, "action added.")
    
    def main(self, context):
        self.add_action(context)

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}


class AddArmatureConstraintsOperator(bpy.types.Operator):
    """Add Armature Constraints Operator"""
    bl_idname = "scene.add_armature_constraints_operator"
    bl_label = "Add Armature Constraints"
    
    # Bones translation (<bvh_bone_name>, <dest_armature_bone_name>)
    bones_names = {
    "lFoot": "foot_l",
    "lShin": "calf_l",
    "lThigh": "thigh_l",
    "hip": "pelvis",
    "abdomen": "spine_02",
    "chest": "spine_03",
    "lCollar": "clavicle_l",
    "lShldr": "upperarm_l",
    "lForeArm": "lowerarm_l",
    "lHand": "hand_l",
    "neck": "neck_01",
    "head": "head",
    "rCollar": "clavicle_r",
    "rShldr": "upperarm_r",
    "rForeArm": "lowerarm_r",
    "rHand": "hand_r",
    "rThigh": "thigh_r",
    "rShin": "calf_r",
    "rFoot": "foot_r",
    }
    
    """
    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    """
    
    def add_constraint(self, context, origin_bone, target_bone):
        print("Adding constraint to ", target_bone, " from ", origin_bone)
        target_bone.constraints.new('COPY_ROTATION')
        rot_constraint = target_bone.constraints[0]
        rot_constraint.target = context.scene.objects[context.scene.origin_armature]
        rot_constraint.subtarget = origin_bone.name

    def add_constraints(self, context):
        print("Adding constraints...")
        for bvh_bone in context.scene.objects[context.scene.origin_armature].pose.bones:
            #print(bvh_bone)
            try:
                self.target_bone_name = self.bones_names[bvh_bone.name]
            except:
                print(bvh_bone.name, " not listed")
            else:
                print(bvh_bone.name, " OK")
                self.add_constraint(context, bvh_bone, context.scene.objects[context.scene.target_armature].pose.bones[self.target_bone_name])

    def main(self, context):
        self.add_constraints(context)

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}


class CopyActionOperator(bpy.types.Operator):
    """Copy Action Operator"""
    bl_idname = "scene.copy_action_operator"
    bl_label = "Copy Action"
    
    
    """
    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    """
    
    def copy_action(self, context):
        print("Copying action...")
        context.scene.objects.active = context.scene.objects[context.scene.target_armature]
        bpy.ops.object.mode_set(mode='POSE')
        bpy.ops.pose.select_all(action='SELECT')
        
        origin_action = context.blend_data.actions[context.scene.origin_armature]
        dest_action = context.blend_data.actions[context.scene.imported_action_name]
        print("Origin action:", origin_action)
        print("Target action:", dest_action)
        
        try:
            context.scene.objects.active.animation_data.action = dest_action
        except:
            context.scene.objects.active.animation_data_create()
            context.scene.objects.active.animation_data.action = dest_action
        
        context.scene.frame_current = context.scene.frame_start
        for i_frame in range(context.scene.frame_start, context.scene.frame_end + 1):
            context.scene.frame_set(i_frame)
            if (i_frame == context.scene.frame_start) or (i_frame == context.scene.frame_end) or ((i_frame-1) % context.scene.imported_action_keyframes == 0):
                context.scene.update()
                bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_VisualRot')
        
    def main(self, context):
        self.copy_action(context)
        print("Action copied.")

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}


class ReadArmaturesOperator(bpy.types.Operator):
    """Read Armatures Operator"""
    bl_idname = "scene.read_armatures_operator"
    bl_label = "Read Armatures"
    
    
    """
    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    """
    
    def read_armatures(self, context):
        print("Reading armatures...")
        for o_bone in context.scene.objects[context.scene.origin_armature].pose.bones:
            current_item = context.scene.origin_bones.add()
            current_item.name = o_bone.name
        for t_bone in context.scene.objects[context.scene.target_armature].pose.bones:
            current_item = context.scene.target_bones.add()
            current_item.name = t_bone.name
        print(context.scene.origin_bones)
        print(context.scene.target_bones)

    def main(self, context):
        self.read_armatures(context)

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

class UpdateArmatureObjectsOperator(bpy.types.Operator):
    """Update Armature Objects Operator"""
    bl_idname = "scene.update_armature_objects_operator"
    bl_label = "Update Lists"
    
    """
    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    """
    
    def main(self, context):
        context.scene.armature_objects.clear()
        for object in bpy.context.scene.objects:
            if object.type == "ARMATURE":
                my_item = context.scene.armature_objects.add()
                my_item.name = object.name
                my_item.value = object.name

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(ArmatureObject)
    bpy.utils.register_class(UpdateArmatureObjectsOperator)
    bpy.utils.register_class(PoseBoneListItem)
    bpy.utils.register_class(ReadArmaturesOperator)
    bpy.utils.register_class(AddArmatureConstraintsOperator)
    bpy.utils.register_class(AddActionOperator)
    bpy.utils.register_class(CopyActionOperator)
    bpy.utils.register_class(BVH2ActionTransferOperator)
    bpy.utils.register_class(BVH2ActionPanel)
    
    bpy.types.Scene.armature_objects = bpy.props.CollectionProperty(type=ArmatureObject)
    bpy.types.Scene.origin_bones = bpy.props.CollectionProperty(type=PoseBoneListItem)
    bpy.types.Scene.target_bones = bpy.props.CollectionProperty(type=PoseBoneListItem)

    bpy.ops.scene.update_armature_objects_operator()

def unregister():
    bpy.utils.unregister_class(BVH2ActionPanel)
    bpy.utils.unregister_class(BVH2ActionTransferOperator)
    bpy.utils.unregister_class(CopyActionOperator)
    bpy.utils.unregister_class(AddActionOperator)
    bpy.utils.unregister_class(AddArmatureConstraintsOperator)
    bpy.utils.unregister_class(ReadArmaturesOperator)
    bpy.utils.unregister_class(PoseBoneListItem)
    bpy.utils.unregister_class(UpdateArmatureObjectsOperator)
    bpy.utils.unregister_class(ArmatureObject)

if __name__ == "__main__":
    register()

